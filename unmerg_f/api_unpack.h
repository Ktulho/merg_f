#pragma once

#include <sal.h>
#include <windows.h>

extern "C" __declspec(dllexport) unsigned char __cdecl UNPACK_divide(_In_ const wchar_t* path, _In_ const wchar_t* filename);