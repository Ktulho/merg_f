#include "merg_f.h"

struct parameters prm;

void set_path(wchar_t *path)
{
	wcscpy_s(prm.path, path);
	add_backslash(prm.path);
}


void set_dirname_from_fname()
{
	wchar_t *pnt = wcsrchr(prm.filename, L'\\');
	if (pnt != NULL)
	{
		wcsncpy_s(prm.path, prm.filename, pnt - prm.filename + 1);
	}
}


void set_fname_from_dirname()
{
	size_t pos = wcslen(prm.path) - 1;
	prm.path[pos] = L'\0';
	wchar_t *pnt = wcsrchr(prm.path, L'\\');
	if (pnt == NULL)
	{
		wcsncpy_s(prm.filename, prm.path, wcsrchr(prm.path, L':') - prm.path);
		wcscat_s(prm.filename, L".mrg");
	}
	else
	{
		wcs2str(prm.filename, pnt + 1, L".mrg");
	}
	prm.path[pos] = L'\\';
}


void set_mask_exclusion(wchar_t *mask)
{
	wchar_t full_path[MAX_PATH] = { 0 };
	if (mask[0] == L'\\')
	{
		wcscat_s(full_path, mask + 1);
	}
	else
	{
		wcscat_s(full_path, mask);
	}
	wcscpy_s(prm.mask_exclusion, full_path);

}

unsigned char pars_param(const int argc, wchar_t* argv[])
{
	if (argc <= MIN_NUMBER_PARAM)
	{
		return SHOW_HELP;
	}

	if (wcscmp(L"-m", argv[1]) == 0)
	{
		prm.is_merging = true;
		prm.include_subdir = false;
		set_path(argv[2]);
	}
	else if (wcscmp(L"-mr", argv[1]) == 0)
	{
		prm.is_merging = true;
		prm.include_subdir = true;
		set_path(argv[2]);
	}
	else if (wcscmp(L"-d", argv[1]) == 0)
	{
		prm.is_merging = false;
		wcscpy_s(prm.filename, argv[2]);
	}

	int i = 3;
	while (i < argc)
	{
		//if (wcscmp(L"-i", argv[i]) == 0)
		//{
		//	wcscpy_s(parameters.inclusion_mask, argv[++i]);
		//}
		//else
		if (wcscmp(L"-e", argv[i]) == 0)
		{
			if (prm.mask_exclusion[0] == L'\0')
			{
				set_mask_exclusion(argv[++i]);
			}
			
		}
		else if (prm.is_merging)
		{
			wcscpy_s(prm.filename, argv[i]);
		}
		else
		{
			set_path(argv[i]);
		}
		++i;
	}

	if (prm.is_merging)
	{
		if (wcslen(prm.filename) == 0)
		{
			set_fname_from_dirname();
		}
		return before_merger();
	}
	else
	{
		if (wcslen(prm.path) == 0)
		{
			set_dirname_from_fname();
		}
		return before_divide();
	}
}

int wmain(int argc, wchar_t* argv[])
{
	_setmode(_fileno(stdout), _O_U16TEXT);
	int result = pars_param(argc, argv);
	switch (result)
	{
	case 0:
		result = 0;
		break;
	case ERROR_FIRST_PARAM:
		wprintf(STR_ERROR_FIRST_PARAM, argv[1]);
		result = ERROR_FIRST_PARAM;
		break;
	case ERROR_NO_FOUND_PATH:
		result = ERROR_NO_FOUND_PATH;
		break;
	case ERROR_NOT_CREATE_FILE:
		result = ERROR_NOT_CREATE_FILE;
		break;
	case ERROR_NOT_FOLDER:
		result = ERROR_NOT_FOLDER;
		break;
	case ERROR_NOT_OPEN_FILE:
		result = ERROR_NOT_OPEN_FILE;
		break;
	case ERROR_NOT_FILE:
		wprintf(STR_ERROR_NOT_FILE);
		result = ERROR_NOT_FILE;
		break;
	case ERROR_NOT_CREATE_DIR:
		result = ERROR_NOT_CREATE_DIR;
		break;
	case SHOW_HELP:
		wprintf(STR_SHOW_HELP);
		result = SHOW_HELP;
		break;
	default:
		wprintf(STR_ERROR_UNKNOWN);
		result = -1;
		break;
	}
	//system("pause");
	return result;
}