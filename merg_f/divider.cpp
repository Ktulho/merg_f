#include "merg_f.h"

// create directories and subdirectories
unsigned char create_dir(const wchar_t *path)
{
	wchar_t cur_path[MAX_PATH] = { 0 };
	size_t len_path = wcslen(path);
	wcscpy_s(cur_path, path);
	wchar_t *ptr = cur_path;
	while ((wcslen(cur_path) > 0) && (_wmkdir(cur_path) != 0))
	{
		ptr = wcsrchr(cur_path, L'\\');
		if (ptr == NULL)
		{
			wprintf(STR_ERROR_NOT_CREATE_DIR, path);
			return ERROR_NOT_CREATE_DIR;
		}
		cur_path[ptr - cur_path] = L'\0';
	}
	if (wcslen(cur_path) == len_path)
	{
		return 0;
	}
	cur_path[ptr - cur_path] = L'\\';
	size_t len_cur_puth;
	while ((len_cur_puth = wcslen(cur_path)) <= len_path )
	{
		if (_wmkdir(cur_path) != 0)
		{
			wprintf(STR_ERROR_NOT_CREATE_DIR, path);
			return ERROR_NOT_CREATE_DIR;
		}
		cur_path[len_cur_puth] = L'\\';
	}
	return 0;
}


//if necessary, creates a subdirectory for the file
unsigned char creates_subdirectory(wchar_t fname[], const wchar_t out_path[])
{
	wchar_t *ptr = wcsrchr(fname, L'\\');
	if (ptr != nullptr)
	{
		wchar_t _path[MAX_PATH] = { 0 };
		wcs2str(_path, out_path, fname);
		_path[wcsrchr(_path, L'\\') - _path] = L'\0';
		if (dir_exist(_path) != 0)
		{
			unsigned char result = create_dir(_path);
			if (result != 0)
			{
				return result;
			}
		}
	}
	return 0;
}


// divide the merged file
unsigned char divide()
{
	FILE *fin, *fout;
	if (_wfopen_s(&fin, prm.filename, L"rb") != 0)
	{
		wprintf(STR_ERROR_NOT_OPEN_FILE, prm.filename);
		return ERROR_NOT_OPEN_FILE;
	}

	char* buf = (char*)malloc(SIZE_BUF);
	const _fsize_t size = file_size(prm.filename);
	while ((feof(fin) == 0) && (_ftelli64(fin) < size))
	{
		wchar_t fname[MAX_PATH] = { 0 };
		for (unsigned short i = 0; (fname[i] = fgetwc(fin)) != L'\0'; ++i);

		if (creates_subdirectory(fname, prm.path) != 0)
		{
			continue;
		}

		_fsize_t size_infile;
		fread(&size_infile, sizeof(_fsize_t), 1, fin);

		wchar_t full_fname[MAX_PATH] = { 0 };
		wcs2str(full_fname, prm.path, fname);
		if (_wfopen_s(&fout, full_fname, L"wb") != 0)
		{
			wprintf(STR_ERROR_NOT_CREATE_FILE, fname);
			continue;
		}

		for (_fsize_t read_b; size_infile > 0; size_infile -= read_b)
		{
			read_b = SIZE_BUF <= size_infile ? SIZE_BUF : size_infile;
			size_t num_b = fread(buf, 1, read_b, fin);
			if (num_b > 0)
			{
				fwrite(buf, 1, num_b, fout);
			}
		}
		wprintf(L"%s\n", fname);
		fclose(fout);
	}
	fclose(fin);
	free(buf);
	return 0;
}


unsigned char before_divide()
{
	unsigned char result;
	if ((result = file_exist(prm.filename)) == 0)
	{
		if ((dir_exist(prm.path) == 0) || ((result = create_dir(prm.path)) == 0))
		{
			result = divide();
		}
	}
	return result;
}
