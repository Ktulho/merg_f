#pragma once

#include <iostream>
#include "windows.h"
#include <io.h>
#include <fcntl.h>

#define SIZE_BUF 1024 * 128
#define MIN_NUMBER_PARAM 2

#define MERGE 0
#define DIVIDE 1

#define ERROR_FIRST_PARAM 1
#define ERROR_NO_FOUND_PATH 2
#define ERROR_NOT_CREATE_FILE 3
//#define ERROR_WRONG_NUMBER_PARAM 4
#define ERROR_NOT_FOLDER 5
#define ERROR_NOT_OPEN_FILE 6
#define ERROR_NOT_FILE 7
#define ERROR_NOT_CREATE_DIR 8
#define SHOW_HELP 9

#define STR_ERROR_FIRST_PARAM L"�������� �������� %s. ������ �������� ������ ���� -m (����������) ��� -d (���������).\n"
#define STR_ERROR_NO_FOUND_PATH L"�� ����� ����: %s.\n"
#define STR_ERROR_NOT_CREATE_FILE L"���������� ������� ����: %s.\n"
//#define STR_ERROR_WRONG_NUMBER_PARAM L"�������� ���������� ����������. �������� %i ��������(�), ������ ���� %i.\n"
#define STR_ERROR_NOT_FOLDER L"%s - ���� ������ ��������� �� �����.\n"
#define STR_ERROR_NOT_OPEN_FILE L"������ �������� �����: %s.\n"
#define STR_ERROR_NOT_FILE L"������ �������� ������ �������� ���� � ����� (�� � �����).\n"
#define STR_ERROR_NOT_CREATE_DIR L"���������� ������� �������: %s.\n"
#define STR_ERROR_UNKNOWN L"����������� ������.\n"
#define STR_SHOW_HELP L"������ �������������:\n" \
					  L"merg_f -m folder [file_name]  - ����������� ���� ������ � ����� folder � ���� file_name.\n"\
			    	  L"                                ���� ��� ����� �� �������, �� �� ����� ���������� folder.mrg.\n"\
					  L"merg_f -mr folder [file_name] - ����������� ���� ������ � ��������� � ����� folder � ���� file_name.\n"\
			    	  L"                                ���� ��� ����� �� �������, �� �� ����� ���������� folder.mrg.\n"\
					  L"megr_f -d file_name [folder]  - ���������� ������ �� ����� file_name � ����� folder.\n"\
					  L"                                ���� ����� �� �������, �� ����� ����� ��������� � ������� ����� file_name.\n"

#ifndef MERG_F_H
#define MERG_F_H

struct parameters {
	bool is_merging;
	bool include_subdir;
	wchar_t filename[MAX_PATH] = { 0 };
	wchar_t path[MAX_PATH] = { 0 };
	wchar_t inclusion_mask[MAX_PATH] = { 0 };
	wchar_t mask_exclusion[MAX_PATH] = { 0 };
	FILE *ptr_file;
} ;

extern struct parameters prm;

#endif

void wcs2str(wchar_t *out_str, const wchar_t *str1, const wchar_t *str2);

void add_backslash(wchar_t *path);

unsigned char dir_exist(const wchar_t *path);

unsigned char file_exist(const wchar_t *path);

_fsize_t file_size(const wchar_t *path);

unsigned char before_merger();

unsigned char before_divide();

