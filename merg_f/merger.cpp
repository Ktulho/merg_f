#include "merg_f.h"

void fill_list_exclusion(wchar_t **list_exclusion, size_t &size_list, size_t &current_pos, const wchar_t current_folder[])
{
	wchar_t search_folder[MAX_PATH] = { 0 };
	wchar_t *slash = wcsrchr(prm.mask_exclusion, L'\\');
	if (slash != nullptr)
	{
		wcsncpy_s(search_folder, prm.mask_exclusion, slash - prm.mask_exclusion + 1);
	}
	else
	{
		wcscpy_s(search_folder, L".\\");
	}

	wchar_t search_folder_mask[MAX_PATH] = { 0 };
	if (current_folder[0] != L'\0')
	{
		wcscpy_s(search_folder, current_folder);
		wcs2str(search_folder_mask, search_folder, L"*");
	}
	else
	{
		if (slash != nullptr)
		{
			wcscpy_s(search_folder_mask, prm.mask_exclusion);
		}
		else
		{
			wcs2str(search_folder_mask, L".\\", prm.mask_exclusion);
		}
	}

	WIN32_FIND_DATAW wfd;
	HANDLE const hFind = FindFirstFileW(search_folder_mask, &wfd);
	if (INVALID_HANDLE_VALUE != hFind)
	{
		do
		{
			if ((_wcsicmp(&wfd.cFileName[0], L".") == 0) || (_wcsicmp(&wfd.cFileName[0], L"..") == 0))
			{
				continue;
			}
			if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				wchar_t _current_folder[MAX_PATH] = { 0 };
				wcs2str(_current_folder, search_folder, &wfd.cFileName[0]);
				add_backslash(_current_folder);
				fill_list_exclusion(list_exclusion, size_list, current_pos, _current_folder);
			}
			else
			{
				if (current_pos == size_list)
				{
					size_list += 128;
					list_exclusion = (wchar_t **)realloc(list_exclusion, size_list);
				}
				list_exclusion[current_pos] = (wchar_t *)calloc(MAX_PATH, sizeof(wchar_t));
				_wfullpath(list_exclusion[current_pos], search_folder, MAX_PATH);
				wcscat_s(list_exclusion[current_pos], MAX_PATH, &wfd.cFileName[0]);
				++current_pos;
			}
		} while (NULL != FindNextFileW(hFind, &wfd));
		FindClose(hFind);
	}
}


unsigned char append_file(const wchar_t relative_path[])
{
	FILE *fin;
	char* buf = (char*)malloc(SIZE_BUF);
	if (buf == NULL)
	{
		exit(-1);
	}
	wchar_t in_full_fname[MAX_PATH] = { 0 };
	wcs2str(in_full_fname, prm.path, relative_path);
	if (_wfopen_s(&fin, in_full_fname, L"rb") != 0)
	{
		wprintf(STR_ERROR_NOT_OPEN_FILE, relative_path);
		free(buf);
		return ERROR_NOT_OPEN_FILE;
	}

	size_t len = wcslen(relative_path) + 1;
	fwrite((char*)relative_path, sizeof(wchar_t), len, prm.ptr_file);

	size_t size_file = file_size(in_full_fname);
	fwrite((char*)&size_file, sizeof(size_t), 1, prm.ptr_file);

	while (!feof(fin))
	{
		size_t num_b = fread(buf, 1, SIZE_BUF, fin);
		if (num_b > 0)
		{
			fwrite(buf, 1, num_b, prm.ptr_file);
		}
	}

	wprintf(L"%s\n", relative_path);
	free(buf);
	fclose(fin);
	return 0;
}


// merge files
unsigned char merging(const wchar_t current_folder[], wchar_t **list_exclusion, size_t count)
{
	WIN32_FIND_DATAW wfd;

	wchar_t search_folder_mask[MAX_PATH] = { 0 };
	wchar_t search_folder[MAX_PATH] = { 0 };
	wcscpy_s(search_folder, prm.path);
	if (current_folder[0] != L'\0')
	{
		wcscat_s(search_folder, current_folder);
	}
	wcs2str(search_folder_mask, search_folder, L"*");


	HANDLE const hFind = FindFirstFileW(search_folder_mask, &wfd);
	if (INVALID_HANDLE_VALUE != hFind)
	{
		do
		{
			if ((_wcsicmp(&wfd.cFileName[0], prm.filename) == 0) || (_wcsicmp(&wfd.cFileName[0], L".") == 0) || (_wcsicmp(&wfd.cFileName[0], L"..") == 0))	continue;

			if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
			{
				if (prm.include_subdir)
				{
					wchar_t _current_folder[MAX_PATH] = { 0 };
					wcs2str(_current_folder, current_folder, &wfd.cFileName[0]);
					add_backslash(_current_folder);
					merging(_current_folder, list_exclusion, count);
				}
			}
			else
			{
				wchar_t full_fname[MAX_PATH] = { 0 };
				_wfullpath(full_fname, search_folder, MAX_PATH);
				wcscat_s(full_fname, &wfd.cFileName[0]);
				bool is_merge = true;
				for (size_t i = 0; i < count; ++i)
				{
					if (_wcsicmp(list_exclusion[i], full_fname) == 0)
					{
						is_merge = false;
						break;
					}
				}
				if (!is_merge) continue;

				if (current_folder[0] != L'\0')
				{
					wchar_t relative_path[MAX_PATH] = { 0 };
					wcs2str(relative_path, current_folder, &wfd.cFileName[0]);
					append_file(relative_path);
				}
				else
				{
					append_file(&wfd.cFileName[0]);
				}
			}
		} while (NULL != FindNextFileW(hFind, &wfd));
		FindClose(hFind);
	}
	return 0;
}

unsigned char before_merger()
{
	unsigned char result = dir_exist(prm.path);
	if (result != 0)
	{
		return result;
	}

	wchar_t out_fullfname[MAX_PATH] = { 0 };
	wcs2str(out_fullfname, prm.path, prm.filename);
	if (_wfopen_s(&(prm.ptr_file), out_fullfname, L"wb") != 0)
	{
		wprintf(STR_ERROR_NOT_CREATE_FILE, prm.filename);
		return ERROR_NOT_CREATE_FILE;
	}
	wprintf(L"%s\n", out_fullfname);

	size_t size_list = 128;
	size_t current_pos = 0;
	wchar_t **list_exclusion = (wchar_t **)calloc(size_list, sizeof(wchar_t));
	fill_list_exclusion(list_exclusion, size_list, current_pos, L"");

	result = merging(L"", list_exclusion, current_pos);

	for (size_t i = 0; i < current_pos; ++i)
	{
		free(list_exclusion[i]);
	}
	free(list_exclusion);
	fclose(prm.ptr_file);
	return result;
}