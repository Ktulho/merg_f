#include "merg_f.h"


//creates a string out_str from str1 and str2
void wcs2str(wchar_t *out_str, const wchar_t *str1, const wchar_t *str2)
{
	wcscpy_s(out_str, MAX_PATH, str1);
	wcscat_s(out_str, MAX_PATH - wcslen(out_str), str2);
}


// if there is no backslash at the end of the line, then add it
void add_backslash(wchar_t *path)
{
	if (wcslen(path) != (wcsrchr(path, L'\\') - path + 1))
	{
		wcscat_s(path, MAX_PATH, L"\\");
	}
}


// return 0 if the directory exists
unsigned char dir_exist(const wchar_t *path)
{
	struct _stat buf;
	if (_wstat(path, &buf) != 0)
	{
		wprintf(STR_ERROR_NO_FOUND_PATH, path);
		return ERROR_NO_FOUND_PATH;
	}
	if (!(buf.st_mode & _S_IFDIR))
	{
		wprintf(STR_ERROR_NOT_FOLDER, path);
		return ERROR_NOT_FOLDER;
	}
	return 0;
}


// return 0 if the file exists
unsigned char file_exist(const wchar_t *path)
{
	struct _stat buf;
	if (_wstat(path, &buf) != 0)
	{
		return ERROR_NO_FOUND_PATH;
	}
	if (buf.st_mode & _S_IFDIR)
	{
		return ERROR_NOT_FILE;
	}
	return 0;
}


_fsize_t file_size(const wchar_t *path)
{
	struct _stat buf;
	if (_wstat(path, &buf) != 0)
	{
		return 0;
	}
	return buf.st_size;
}

